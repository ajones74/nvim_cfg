# Neovim Configuration File(s)

## Required Files

* `coc-settings.json`
* `init.vim`

These files are to be installed on Linux Mint in the `~/.config/nvim/` directory


## Install pre-requisites

* `sudo apt install nodejs`
* `sudo apt install npm`
* `sudo apt install bear`

## Enable `snapd` on Linux Mint 20:
* `sudo rm /etc/apt/preferences.d/nosnap.pref`

## Use `snap` to install `ccls` -- C/C++ LSP framework:
* `sudo apt install snapd`
* `sudo snap install ccls --classic`

## Install `vim-plug` for the neovim plugin manager

* Reference source: `https://github.com/junegunn/vim-plug`

* Previously, I've used `dein`, but it doesn't work on Ubuntu 20.04 -- 
time to try `vim-plug` (spoiler alert: *it works*!)

* Run this script:
````
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
````

* Fire up neovim, and run the `:PlugInstall` command to install the plugins found in `init.vim` file.


## Build the compilation database for ccls

* `cd <top-level-directory>`
* Instead of using make by itself (`make`) use "bear" instead: `bear make`.
** Using "bear", the generated output is a file named `compile_commands.json`

## Use a custom `.ccls` file when `compile_commands.json` is not enough

* So far, I haven't had to use a `.ccls` file for my C-based projects,
but it's almost a necessity to use this file for C++-based projects, 
as detailed here:

https://github.com/MaskRay/ccls/wiki/Project-Setup#ccls-file

* I assume my C++-centric .ccls file would look like:

````
clang++

# use clang for C-specific projects, instead...

# The following will process `.h` files as C++ header-files:

%h -x
%h c++-header
````


